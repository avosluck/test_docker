FROM ubuntu
RUN apt update && apt-get install -y  openssh-client && apt-get install -y curl && apt-get install -y net-tools && apt install -y apt-get install fakeroot -y
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["while true; do sleep 300; done"]
